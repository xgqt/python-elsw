# Elsw

Tool providing a nice way to view the Portage "world" file.

## Options

``` text
  -h, --help            show this help message and exit
  -V, --version         show program's version number and exit
  -a, --all             List all available packages
  -i, --installed       List @installed packages
  -w, --world           List @world set(s) packages
  -s, --sets            List non-@world packages
  -e EXCLUDE, --exclude EXCLUDE
                        Exclude package categories from being listed (pass in a delimited list string)
```

## License

Copyright (c) 2019-2024, Maciej Barć <xgqt@xgqt.org>
Licensed under the GNU GPL v2 License

SPDX-License-Identifier: GPL-2.0-or-later
